# PyMod-YT

Final Project on Computational Physics II
Molecular Dynamics tries to describe the properties of an assembly of molecules in term of their structure and the microscopic interactions between them, such as forces. It is an extremely powerful numerical approach to describe many-body systems.. The simulation is done at constant energy with this modele PyMoD.  Boundary conditions are periodic to imitate an infinite system and reduce surface effects with respect to a box. 

Learning about Molecular Dynamics is best achieved by looking at an example. Enjoy!

V(r) = 4 \epsilon \left( \frac{\sigma}{r}^{12} - \frac{\sigma}{r}^6 \right)

Landau, R. H., Páez, M. J., & Bordeianu, C. C. (2015). Computational physics: problem solving with Python, p. 398. John Wiley & Sons. 

Installation:

$ sudo python setup.py install

The pamoldyn.py script may be either executed directly from the command line or loaded as a Python module. (by Duncan)

For help call:

$ pymod -h

It also can be run in parallel like this:
mpirun -np 4 pymod -N.....

Enjoy