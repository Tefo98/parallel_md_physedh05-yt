#!/usr/bin/env gpaw-python

"""Test for Molecular Dynamcs paralellized code (PyMoD)
simulation of a random box of argon"""



def main():
    """Box of 20 lenght_box, 500 particles, 0.001 sec total time, f
    for total time, argon's like distribution and 0.05 eV temperature"""

    moldyn = P_mod(number_particles = 500, 
                  length_box = 20, 
                  temperature = 0.05, 
                  t_max = 0.1, 
                  distribution = 'argon', 
                  name_file = None)
    moldyn.get_initial_forces()
    moldyn.get_energies()
    moldyn.Storage()
    

MOLDYN = main()
if __name__ == '__main__':
    main()
