from pamoldyn import Pmod
a = Pmod()
#a = Verlet(number_particles = 100, length_box=30, temperature = 0.05, t_max = 0.001, distribution = 'argon', name_file = 'Parameters.txt')
a.read_filename('Parameters.txt')
a.get_initial_forces()
a.get_energies()
a.plot_energies()
a.plotting_molecules()