https://gitlab.com/Tefo98/parallel_md_physedh05-yt"""Parallel Molecular Dynamics"""
import random as rd
import time
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpi4py import MPI
from design import parameters
from design import headings
from design import logo
from design import printing_results
from argparse import ArgumentParser


class MD(object):
    """Molecular dynamics class
        
        position              array containing xyz position of molecules
        velocities            array containing xzy velocities of molecules
        forces                array containing xyz forces of molecules
        kinetic_energy        array containing ke of the system at all times
        potential_energy      array containing pe of the system at all times
        distribution          initial arrangment of molecules
        temperature           initial temperature of the system
        number_particles      total generated number of particles
        length_box            size of the box in all directions (symmetric)
        t_max                 total time of the simulation
        current_t             specific point of time in simulation
        cut_ratio             maximum distance for considering potentials
        energy_cut_ratio      potential energy related to cut_ratio
        t_step                time for periods of integration
        name_file             txt containing initial parameters
        current_kin_energies  kinetic energy at a given time
        current_pot_energies  potential energy at a given time"""
        
    
    def __init__(self, number_particles=None, length_box=None, temperature=0.05,
                 t_max=None, distribution='argon', name_file='Parameters.txt',
                 t_step=None):
        self.positions = None
        self.velocities = None
        self.forces = None
        self.kinetic_energies = None
        self.potential_energies = None
        self.distribution = distribution
        self.temperature = temperature
        self.number_particles = number_particles
        self.length_box = length_box
        self.t_max = t_max
        self.current_t = None
        self.cut_ratio = None
        self.energy_cut_ratio = None
        self.t_step = t_step
        self.name_file = name_file
        self.current_kin_energies = None
        self.current_pot_energies = None
    #This code create intial position and velocities in a 2D box with finite
    #length and given temperature
    #This code works until 15000 steps in
    #time for time steps of 0.001
    def read_filename(self, name='Parameters.txt'):
        """read initial parameters"""
        data = open(str(name), "r")
        self.number_particles = int(data.readline().split()[1])
        self.length_box = int(data.readline().split()[1])
        self.temperature = float(data.readline().split()[1])
        self.t_max = float(data.readline().split()[1])
        self.distribution = data.readline().split()[1]
        self.t_step = float(data.readline().split()[1])

    def initial_positions(self):    #
        """Here we generate the initial position according to the inputs"""
        self.cut_ratio = self.length_box/2
        if self.distribution == "argon" and self.positions is None:
            p_p = []
            la_si = 8.
            l_l = 0
            lattice_position = ([0., 0., 0.], [la_si/2., 0., la_si/2.],
                                [0., la_si/2., la_si/2.], [la_si/2., la_si/2., 0.])
            for i in range(0, self.length_box, int(la_si)):
                for j in range(0, self.length_box, int(la_si)):
                    for k in range(0, self.length_box, int(la_si)):
                        initial_position = np.array(([i, j, k]))
                        for l_l in range(0, 4, 1):
                            p_p.append(initial_position + lattice_position[l_l])
            self.positions = np.asarray(p_p)
            self.positions += self.length_box * 0.2
            self.length_box += 0.4 * self.length_box
            self.number_particles = len(self.positions)

        else:    #in the future it will implement more distributions
            raise 'Not implemented'


    def get_initial_positions(self):
        """generate initial positions"""
        if self.positions is None:
            self.initial_positions()

    def gauss(self, v_v, t_t):
        """gauss function"""
        return np.exp(-v_v*v_v/(2*t_t)) / ((2*np.pi()*t_t)**0.5)

    def initial_velocities(self):
        """create initial velocities"""
        if self.velocities is None:
            v_v = []
            for j in range(len(self.positions)):
                v_v.append([rd.gauss(0, self.temperature),
                            rd.gauss(0, self.temperature),
                            rd.gauss(0, self.temperature)])
                j = j + 1
            self.velocities = np.asarray(v_v)
    def get_initial_velocities(self):
        """calculate initial velocities"""
        if self.velocities is None:
            self.initial_velocities()
            self.kinetic_energies = [self.get_kinetic_energy()]
    def get_initial_forces(self):
        """calculate initial forces"""
        if self.forces is None:
            if self.velocities is None:
                if self.positions is None:
                    self.get_initial_positions()
                self.get_initial_velocities()
            self.forces, e_n = self.update_forces([0, len(self.positions) - 1])
            self.potential_energies = [e_n]
    def plotting_molecules(self):
        """ plot particles positions"""
        world = MPI.COMM_WORLD
        if world.rank == 0:
            plt.figure()
            plt.title("Argon Lattice t=0")
            a_x = plt.axes(projection="3d")
            a_x.grid(True)
            for i in range(0, self.number_particles, 1):
                a_x.scatter(self.positions[i, 0], self.positions[i, 1], self.positions[i, 2])
            plt.title("Snapshot of molecules at t="+str(self.t_max)+" with "+
                      str(self.number_particles)+" particles")
            plt.show()
        return()

    def storage_data(self):
        """save the position x y and velocities vx vy in a .txt files called position"""
        name = str(self.t_step) + '_' + str(self.t_max)
        data2 = open("pos_vel_" + str(name) + ".txt", "w")
        data2.write("X"+"\t"+"Y"+"\t"+"Z"+"\t"+"Vx"+"\t"+"Vy"+"\t"+"Vz"+"\n")
        for k in range(0, len(self.positions), 1):
            data2.write(str(self.positions[k, 0])+"\t"+str(self.positions[k, 1])+"\t"+
                        str(self.positions[k, 2])+"\t"+str(self.velocities[k, 0])+"\t"+
                        str(self.velocities[k, 1])+"\t"+str(self.velocities[k, 2])+"\n")
        data2.close()
        return()

    def get_energy_cut_ratio(self):
        """calculate cut-ratio"""
        self.energy_cut_ratio = ((1/self.cut_ratio)**12 - (1/self.cut_ratio)**6)

    def update_forces(self, index):
        """calculate forces using Lennard-John Potential"""
        e_p = 0.
        f_ff = np.copy(self.positions)*0.
        for i in range(index[0], index[1]):
            for j in range(i+1, self.number_particles):
                x_r = self.positions[i, :] - self.positions[j, :]
                #print xr
                r_2 = (x_r*x_r).sum()
                if r_2 <= self.cut_ratio * self.cut_ratio:    #cut ratio
                    r_2i = 1./r_2
                    r_6i = r_2i * r_2i * r_2i
                    f_f = 48. * r_2i * r_6i * (r_6i-0.5)
                    f_ff[i, 0] += f_f * x_r[0]     #Calculating forces i X Y
                    f_ff[j, 0] -= f_f * x_r[0]
                    f_ff[i, 1] += f_f * x_r[1]
                    f_ff[j, 1] -= f_f * x_r[1]
                    f_ff[i, 2] += f_f * x_r[2]
                    f_ff[j, 2] -= f_f * x_r[2]
                    e_p += 4. * (r_6i ** 2. - r_6i)   #calculating Kinetic Energy
        return f_ff, e_p

    def update_positions(self, pos_temp, index):
        """calculate new positions"""
        for i in range(index[0], index[1] + 1):
            pos_temp[i, 0] += self.t_step * (self.velocities[i, 0] +
                                             self.forces[i, 0] * self.t_step * 0.5)
            pos_temp[i, 1] += self.t_step * (self.velocities[i, 1] +
                                             self.forces[i, 1] * self.t_step * 0.5)
            pos_temp[i, 2] += self.t_step * (self.velocities[i, 2] +
                                             self.forces[i, 2] * self.t_step * 0.5)
            if pos_temp[i, 0] <= 0.:    #applying boundary condition to avoid particles to get out
                pos_temp[i, 0] += self.length_box
            if pos_temp[i, 0] >= self.length_box:
                pos_temp[i, 0] -= self.length_box
            if pos_temp[i, 1] <= 0.:
                pos_temp[i, 1] += self.length_box
            if pos_temp[i, 1] >= self.length_box:
                pos_temp[i, 1] -= self.length_box
            if pos_temp[i, 2] <= 0.:
                pos_temp[i, 2] += self.length_box
            if pos_temp[i, 2] >= self.length_box:
                pos_temp[i, 2] -= self.length_box
        return pos_temp
    def update_velocities(self, forces_old_temp, in_dex):
        """verlet's formula until linear correction"""
        vel_temp = np.copy(self.velocities) * 0.
        for i in range(in_dex[0], in_dex[1] + 1):
            vel_temp[i, 0] += (self.forces[i, 0] + forces_old_temp[i, 0]) * self.t_step * 0.5
            vel_temp[i, 1] += (self.forces[i, 1] + forces_old_temp[i, 1]) * self.t_step * 0.5
            vel_temp[i, 2] += (self.forces[i, 2] + forces_old_temp[i, 2]) * self.t_step * 0.5
        return vel_temp

    def read_data(self, name):
        """ read data from txt"""
        file = open(name, 'r')
        f_1 = file.readlines()
        r_1 = []
        i_1 = 0
        v_1 = []
        for line in f_1:
            if i_1 != 0:
                a_a = float(line.split('\t')[0])
                b_b = float(line.split('\t')[1])
                c_c = float(line.split('\t')[2])
                p_p = [a_a, b_b, c_c]
                r_1.append(p_p)
                a_a = float(line.split('\t')[3])
                b_b = float(line.split('\t')[4])
                c_c = float(line.split('\t')[5])
                s_s = [a_a, b_b, c_c]
                v_1.append(s_s)
            i_1 += 1
        self.positions = np.asarray(r_1)
        self.velocities = np.asarray(v_1)
    def get_kinetic_energy(self):
        """Calculating kinetic energy-classical model"""
        k_e = 0.0
        for i in range(len(self.velocities)):
            k_e += ((self.velocities[i] * self.velocities[i]).sum())/2
        return k_e

    def plot_energies(self):
        """plot energies in function of time"""
        t_m = np.arange(0, self.t_max + self.t_step, self.t_step)
        plt.plot(t_m, self.kinetic_energies, "r", label="Kinetic Energy")
        plt.plot(t_m, np.asarray(self.kinetic_energies) +
                 np.asarray(self.potential_energies), "g", label="Total Energy")
        plt.plot(t_m, self.potential_energies, "b", label="Potential Energy")
        plt.legend(loc='center right', shadow=False, fontsize='x-large')
        plt.xlabel("Time")
        plt.title("Energy Drift with time steps of "+
                  str(self.t_step)+" in a total time of "+str(self.t_max))
        plt.ylabel("Energy")
        plt.show()

    def par_pos(self, world):
        """code for parallelizing"""
        index_list = []
        pos_temp = []
        pos_item = []
        size_w = world.size
        index_w = []
        num_w = 0
        for i in range(len(self.positions)):
            if len(pos_item) == 0:
                index_w.append(i)
            pos_item.append(self.positions[i])
            if len(pos_item) >= float(len(self.positions)/size_w) and num_w != size_w-1:
                index_w.append(i)
                pos_temp.append(pos_item)
                index_list.append(index_w)
                pos_item = []
                index_w = []
                num_w += 1
        index_w.append(i)
        pos_temp.append(pos_item)
        index_list.append(index_w)
        pos_temp = np.asarray(pos_temp)
        index_list = np.asarray(index_list)
        pos_element = world.scatter(pos_temp, root=0)
        index_w = world.scatter(index_list, root=0)
        pos_temp = np.copy(self.positions) * 0.
        count_w = 0
        for k in range(0, len(self.positions)):
            if k >= index_w[0] and k <= index_w[1]:
                pos_temp[k] = pos_element[count_w]
                count_w += 1
        return pos_temp, index_w

def read_arguments():
    """Input Argument Parsing"""
    parser = ArgumentParser()
    parser.add_argument('-L', '--length_box',
                        help=' length of the box, symmetric in xyz axis',
                        default=30, type=int)
    parser.add_argument('-N', '--number_particles',
                        help='generated number of particles',
                        default=4*30*30*30, type=int)
    parser.add_argument('-T', '--temperature',
                        help='initial temperature of the system)',
                        type=float, default=0.4)
    parser.add_argument('-tm', '--t_max',
                        help='duration of simulations ',
                        type=float, default=10.0)
    parser.add_argument('-d', '--distribution', type=str,
                        help='initial distribution of particles')
    parser.add_argument('-ts', '--t_step',
                        help='integration time', type=float)

    pargs = parser.parse_args()

    return pargs

class Pmod(MD):
    """molecular dynamics steps"""
    def get_energies(self):
        """ time integration"""
        world = MPI.COMM_WORLD
        t_m = np.arange(self.t_step, self.t_max + self.t_step, self.t_step)
        if world.rank == 0:
            logo()
            parameters(self.number_particles, self.length_box, self.temperature, self.t_max, self.distribution)
            headings()
            start = time.time()
        cont_cont = 0
        for t_t in t_m:
            cont_cont += 1
            #print self.positions
            pos_temp, index = self.par_pos(world=world)
            #print self.velocities
            #print self.forces
            pos_temp = self.update_positions(pos_temp, index) #integrating positions
            #print self.positions
            self.positions *= 0.
            world.Allreduce(sendbuf=pos_temp, recvbuf=self.positions, op=MPI.SUM)
            #print self.positions
            forces_old_temp, ep_temp = self.update_forces(index)
            #calculating forces with new positions
            vel_temp = self.update_velocities(forces_old_temp, index)
            #calculating the verlet's velocities
            e_n = np.array([0.])
            world.Allreduce(sendbuf=np.array([ep_temp]), recvbuf=e_n, op=MPI.SUM)
            #print self.velocities, '------------'
            vel_b = np.copy(self.velocities)
            world.Allreduce(sendbuf=vel_temp, recvbuf=self.velocities, op=MPI.SUM)
            self.velocities += vel_b
            del vel_b
            #print self.velocities
            self.forces *= 0
            world.Allreduce(sendbuf=forces_old_temp, recvbuf=self.forces, op=MPI.SUM)
            if world.rank == 0:
                k_e = self.get_kinetic_energy()
                #calculating Kinetic Energy of current velocities
                self.potential_energies.append(e_n[0])
                #Saving the potential, kinetic and total energies
                self.kinetic_energies.append(k_e)
                #setting initial force for the next step t
                self.current_t = t_t
                self.current_pot_energies = e_n[0]
                self.current_kin_energies = k_e
                printing_results(e_n[0], k_e, t_t, cont_cont)
        if world.rank == 0:
            end = time.time()
            print "Total time of simulation: "+ str(end - start)

def main():
    """Main Executable"""
    args = read_arguments()
    pymod = Pmod(length_box=args.length_box,
                 number_particles=args.number_particles,
                 temperature=args.temperature,
                 t_max=args.t_max,
                 distribution=args.distribution,
                 t_step=args.t_step)
    pymod.get_initial_forces()
    pymod.get_energies()
    from mpi4py import MPI
    world = MPI.COMM_WORLD
    if world.rank == 0:
        pymod.storage_data()
        pymod.plot_energies()


if __name__ == '__main__':
    main()