"""printing design for package"""
import datetime
import math

def logo():
    """prints logo and general information about the package"""
    print"_____________           __________     ____    _______________"
    print"_____  ____   \         _______   \   /   /    ___________   /"
    print"____  /    /  /  _______ ______    \_/   /    __________    /"
    print"___  /__ _/  /   _/    /      /  \__    /  _____     ___/  / "
    print"__     _____/   _/____/      /  /   /  /  __  _  \  / __  /"
    print"  /   /                     /  /   /  /    / /_/ / / /_/ /"
    print" /   /                     /__/   /__/     \____/  \____/"
    print"/___/                                                   "
    print"                                          /v 0.1 , YT/"
    print"------------------------------------------------------------------"
    now = datetime.datetime.now()
    print str(now.strftime("%Y-%m-%d %H:%M:%S"))
    print""
    print"Title: PARALLEL MOLECULAR DYNAMICS"
    print"Mantainer: Raul A. Hidalgo  ---->  raul.hidalgo@yachaytech.edu.ec"
    print"Developer: Stefano R. Meza  ---->  stefano.meza@yachaytech.edu.ec"
    print"Afilation: School of Physical Sciences and Nanotechnology"
    print"---Python version: 2.7.15 on linux2---"
    print"------------------------------------------------------------------"
    return()


def parameters(number_particles, length_box, temperature, t_max, distribution):
    """ initial parameters from txt or users prompt and shows it to the user"""
    number_particles = int(number_particles)
    length_box = int(length_box)
    temperature = float(temperature)
    t_max = float(t_max)
    distribution = distribution
    print""
    print"---------------Setting up established configurations---------------"
    print"                        ___"
    print"                       |   "
    print"                       |Number of particles  ------->"+str(number_particles)
    print"    Chosen             |Lenght of the box    ------->"+str(length_box)
    print"    initial   ----->   |Initial temperature  ------->"+str(temperature)
    print"   parameters          |Time for simulation  ------->"+str(t_max)
    print"                       |Initial Distribution ------->"+str(distribution)
    print"                       |___"
    print""
    print"PLEASE, CALCULATE THE CORRESPONDING ENERGIES"

def headings():
    """printing scheme for energy calculation"""
    print"--------------------Initializating system--------------------------"
    print"                            .........                              "
    print"                            .........                              "
    print"                            .........                              "
    print"                            .........        *Stand by please      "
    print"                            .........                              "
    print"                            .........                              "
    print"                            .........                              "
    print"--------------------------Printing data----------------------------"
    print"___________________________________________________________________"
    print"| n   |time step|   Potential E. |   KInetic E.  |  Total Energy  |"
    print"|-----------------------------------------------------------------|"

def truncate(number, digits):
    """truncates the digits of energies"""
    stepper = pow(10.0, digits)
    return math.trunc(stepper*number)/stepper

def printing_results(e_p, e_k, t_t, i):
    """prints energies, timestep and number of iteration"""
    e_p = truncate(e_p, 9)
    e_k = truncate(e_k, 9)
    e_t = e_p + e_k
    t_t = truncate(t_t, 6)
    print"|"+str(i)+"    "+str(t_t)+ "     "  +str(e_p)+   "     "   +str(e_k)+"    "+str(e_t)+"|"
    print"|-----------------------------------------------------------------|"

def finalizator():
    """prints final output of the code"""
    print"--------------------------Simulation Over-------------------------|"
    print"***Cooling down machine, please stand by"
    print"***Checking data and bugs"
    print"***Energies had been storaged"
    print""
    print""
    print"************************Congratulations****************************|"
    print"**********************Succesfull simulation************************|"