from distutils.core import setup

setup(name='pmod',
      version='1.0',
      description='Molecular dynamics Simulations in Parallel Computing',
      maintainer='Raul Hidalgo Sacoto',
      maintainer_email='raulhidalgos964@gmail.com',
      developer='Stefano Meza Anzules',
      developer_email='stefano.phywork@gmail.com',
      url='https://gitlab.com/Tefo98/parallel_md_physedh05-yt',
      platforms=['unix'],
      scripts=['bin/pymod'],
      packages=['pamoldyn'])
